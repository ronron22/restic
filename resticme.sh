#!/bin/bash 

set -x


PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$HOME/bin
RESTIC_PASSWORD=/root/.restic_password
RESTIC_REPO=/root/.restic_repo
ops="-x"
script_name=$(basename ${0/.sh/})
tmp_file=$(mktemp --suffix $script_name)
lock_file=/var/lock/.${script_name}.exclusivelock

lock() {
	# http://www.kfirlavi.com/blog/2012/11/06/elegant-locking-of-bash-program/
	exec 200>$lock_file

	if flock -n 200 ; then
		return 0 
	else
		return 1
	fi
}

lock || exit 1

# loading credential if exist
if [ -f $RESTIC_REPO ] ; then
	source $RESTIC_REPO
    restic_program="/usr/bin/docker run -v /root/:/root/ --rm -ti restic/restic -p $RESTIC_PASSWORD -r $RESTIC_REPOSITORY"
fi

clean_exit() {
	if [ -f $tmp_file ] ; then
		unlink $tmp_file
	fi
	if [ -f $lock_file ] ; then
		rm -f $lock_file
	fi
}

logme() {
	logger -t $script_name $1
}

function log_and_exec () {
	#https://www.linuxquestions.org/questions/linux-newbie-8
	#/bash-log-and-execute-function-670915/
       	eval "$@";
	logger \[$?\] "$@";
}


trap clean_exit 2 3 9

declare -A tag_and_hash_dict 

tag_and_hash_dict=( ["lib-cyrus"]="/var/lib/cyrus" ["spool-cyrus"]="/var/spool/cyrus" ["www"]="/var/www" ["lib-knot"]="/var/lib/knot" ["lib-ldap"]="/var/lib/ldap" ["lib-mysql"]="/var/lib/mysql" ["etc"]="/etc" ["synapse-config"]="/usr/local/synapse/homeserver.yaml" ["restic-config"]="/root/restic/resticme.sh" )

list_snapshot() {
    #if ! $restic_program -r $REPO snapshots ; then
    if ! $restic_program snapshots ; then
        echo "Unable to list snapshots"
        exit 2
    else
        echo "Check is ok"
    fi	
}

case $1 in
	start)
		for key in ${!tag_and_hash_dict[@]} ; do
            restic_program="/usr/bin/docker run -v /root/:/root/ -v ${tag_and_hash_dict[$key]}:${tag_and_hash_dict[$key]} --rm -ti restic/restic -p $RESTIC_PASSWORD -r $RESTIC_REPOSITORY"
			$restic_program backup $ops --tag $key ${tag_and_hash_dict[$key]}
		done	
	;;	
	clean) 
		$restic_program forget unlock
		$restic_program forget --prune --keep-daily 30
	;;
	check)
		$restic_program check
	;;
	list)
        list_snapshot
	;;
    checkbackup)
        my_date="$(date +%F)"
        nb_snapshot_backuped=$(list_snapshot | grep $my_date | wc -l)
        if (( $nb_snapshot_backuped != ${#tag_and_hash_dict[@]} )) ; then
            logme "some snapshot seem missed on date of $my_date : $nb_snapshot_backuped for ${#tag_and_hash_dict[@]}"
        else
           logme "backup for $my_date is ok"
        fi 
    ;;
    init)
         $restic_program init
    ;;
	*)
		echo "Usage is init|start|clean|check|list|checkbackup"
	;;	
esac	
